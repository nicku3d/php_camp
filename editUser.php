<?php

require_once 'init.php';

$myDB = new Database();
$DAO = new UserDAO($myDB);

//var_dump($_POST);

if(!empty($_POST['email'])
    && !empty($_POST['age'])
    && !empty($_POST['phone'])
    && !empty($_POST['location'])){

    $user = new User();
    $user->login = $_POST['login'];
    $user->email = $_POST['email'];
    $user->age = $_POST['age'];
    $user->phone = $_POST['phone'];
    $user->location = $_POST['location'];

    $result = $DAO->editUser($user);
    if($result) {
        echo 'Dane uzytkownika zmienione';
        echo '</br> <a href="userList.php">Panel zarządzania użytkownikami<a/>';
        echo '</br> <a href="index.php">Wróć do strony głównej!<a/>';
    } else {
        echo 'Wystapił błąd! <br/>';
        var_dump($myDB->getErrorList());
    }
} else {
    echo '</br> <a href="userList.php">Aby usunąć lub zmienić dane użytkownika przejdź tutaj!<a/>';
}