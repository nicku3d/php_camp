<?php
require_once 'init.php';

//jesli uzytkownik Nie jest zalogowany następuje przejscie do strony glownej
if(!isset($_SESSION['login'])){
    header('Location: index.php');
    exit();
}
session_destroy();
echo 'Zostałeś pomyślnie wylogowany.';
echo '</br> <a href="formLogin.php">Zaloguj się!<a/>';