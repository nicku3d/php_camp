<?php
//tutaj beda informacje/profil uzytkownika
require_once 'init.php';


$myDB = new Database();
$DAO = new UserDAO($myDB);

if(isset($_SESSION['login'])) {
    $user = $DAO->getUser($_SESSION['login']);
    echo "Twoje dane: </br>";
    echo 'Login: '. $user->login .'</br>';
    echo 'Email: ' . $user->email .'</br>';
    echo 'Wiek: ' . $user->age .'</br>';
    echo 'Telefon: ' . $user->phone .'</br>';
    echo 'Miejscowość: '. $user->location .'</br>';
    echo '</br> <a href="index.php">Wróć do strony głównej!<a/>';
} else {
    echo 'Nie jesteś zalogowany! </br>';
    echo '</br> <a href="formLogin.php">Zaloguj się!<a/>';
}