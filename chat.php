<?php
require_once 'init.php';


if(isset($_SESSION['login'])) {
    $chatController = new chatController($_SESSION['login'], $_SESSION['key']);

    $chatController->clearChats();
    $chatListString = $chatController->refreshChatList();
    $msgListString = $chatController->refreshMessages();
    $activeChatUsersString = $chatController->refreshChatUsers();
    $allUsersString = $chatController->refreshAllUsers();

    $loginInfo = 'Zalogowano jako: ' . $_SESSION['login'];
} else {
    echo 'Nie jesteś zalogowany.';
    echo '</br> <a href="formLogin.php">Zaloguj się!<a/>';
    die;
}
?>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css" >
</head>
<body>
<div class="login-info">
    <?=$loginInfo ?>
</div>
<div class="d-flex justify-content-center h-75 w-100 container">
<!--    TODO: wyszukiwarka-->
    <div class="overflow-auto users">
        <div class="btn-group-vertical">
                <?=$allUsersString?>
        </div>
    </div>

    <div class="chat d-flex flex-column flex-grow-1 p-2 w-50">
        <div class="open-chats chat-item d-flex flex-row overflow-auto h-10">
            <?=$chatListString;?>
        </div>
        <div class="conversation chat-item h-75 overflow-auto">
            <div id ="chat-users" class="mb-4">
                <?=$activeChatUsersString;?>
            </div>
            <div  id="messages" >
                <?=$msgListString;?>
            </div>
        </div>
        <div class="message-sender chat-item h-10">
            <input type="text" id="message"  class="form-control" name="message" />
            <button type="button" id="sendBtn" class="form-control" onclick="sendMessage()" >Send</button>
        </div>
    </div>

    <div class="menu p-2 w-25" >
        </br> <a href="index.php">Wróć do strony głównej<a/>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="myAjax.js"></script>
<script>
    //Sending msg using Enter instead of Send button
    let btn = document.getElementById("sendBtn");
    let txtMsg = document.getElementById("message");

    document.addEventListener("keyup", function(event){
        if(event.code === "Enter" && txtMsg.value.length !== 0){
            btn.click();
        }
    });
</script>
<script>
    //refreshing messages
    //nie najlepsze rozwiązanie,
    //można by było sprawdzać czy doszły jakieś nowe wiadomości i wtedy dopiero je odświeżać np
    $(document).ready(function() {
        setInterval(function () {
            getMessages();
        }, 5000);
    });
</script>
</body>

