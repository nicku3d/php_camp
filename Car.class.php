<?php


class Car
{
    public $model;
    public $wheelsCount;
    public $doorsCount;
    public $color;
    public $trunkSize;
    public $power;
    public $fuelType;

    public function drive(){
        print 'broom broom <br/>';
    }

    public function stop(){
        print 'stopping <br/>';
    }

    public function honk(){
        print 'honk honk <br/>';
    }


}