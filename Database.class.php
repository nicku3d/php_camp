<?php

class Database
{
    public $db;

    public function __construct()
    {
        //może warto przechowywać w pliku
        $host = 'localhost';
        $dbUser = 'root';
        $dbPass = '';
        $dbName = 'phpcamp';

        $this->db = new mysqli($host, $dbUser, $dbPass, $dbName);

        if($this->db->connect_error) {
            //blad polaczenia
            //nie wyswietla sie błędów z php tylko własne informacje o błędzie
            var_dump($this->db->connect_error);
            die;
        }
    }

    public function __destruct()
    {
        $this->db->close();
    }

    public function getRow($query){
        $result = $this->db->query($query);
        if($result) {
            $return =  $result->fetch_assoc();
            if($result === null){
                return [];
            } else {
                return $return;
            }
        } else {
            return [];
        }
    }

    public function getRows($query){
        $result = $this->db->query($query);
        if($result){
            return $result->fetch_all(MYSQLI_ASSOC);
            //metody phpowe jak im się nie uda zwracają false często nawet jak tego nie pisze w dokumentacji
        } else {
            return [];
        }
    }

    public function query($query) {
        $result = $this->db->query($query);
        if($result){
            return true;
        } else {
            return false;
        }
    }

    public function getError(){
        return $this->db->error;
    }

    public function getErrorList(){
        return $this->db->error_list;
    }


}