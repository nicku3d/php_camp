<?php
require_once 'init.php';


if(!empty($_SESSION['login']) && !empty($_SESSION['key'])) {

    $chatController = new chatController($_SESSION['login'], $_SESSION['key']);

    if (isset($_POST['postPurpose'])) {
        $postPurpose = $_POST['postPurpose'];
        switch ($postPurpose) {
            case 'leaveChat':
                $chatController->leaveChat($_POST['chatID']);

                echo $chatController->refreshChatList();
                break;
            case 'createAndJoin':
                $user = $_POST['user'];
                //TODO: nie powinno być próby dodawania użytkownika jeśli czat nie został stworzony na nowo
                $chatID = $chatController->createChat($user);
                $chatController->joinChat($user, $chatID);
                $chatController->openChat($chatID);
                $chatController->setActiveChat($chatID);

                $json['chat'] = $chatController->refreshChatList();
                $json['messages'] = $chatController->refreshMessages();
                $json['chatUsers'] = $chatController->refreshChatUsers();
                echo json_encode($json);
                break;
            case 'setActiveChat':
                $chatID = $_POST['chatID'];
                $chatController->setActiveChat($chatID);
                $chatController->openChat($chatID);

                $json['chat'] = $chatController->refreshChatList();
                $json['chatUsers'] = $chatController->refreshChatUsers();
                $json['messages'] = $chatController->refreshMessages();
                echo json_encode($json);
                break;
            case 'sendMessage':
                $msg = $_POST['message'];
                $chatController->sendMessage($msg);

                echo $chatController->refreshMessages();
                break;
            case 'getMessages':
                //TODO: czy warto dodać sprawdzanie czy ilość wiadomości użytkownika się nie zmieniła?
                echo $chatController->refreshMessages();
                break;
            case 'closeChat':
                //zamknięcie czatu = zniknięcie go z open-chats, nie usunięcie całkowite
                $chatID = $_POST['chatID'];
                $chatController->closeChat($chatID);
                //$chatController->setActiveChat(-1);

                $json['chat'] = $chatController->refreshChatList();
                $json['messages'] = $chatController->refreshMessages();
                $json['chatUsers'] = $chatController->refreshChatUsers();
                echo json_encode($json);
                break;
        }
    } else {
        echo 'Something went wrong...';
    }
}
