<?php

require_once 'init.php';
//var_dump($_POST);

$myDB = new Database();
$DAO = new UserDAO($myDB);

if(!empty($_POST['login'] && !empty($_POST['pass']))) {
    $login = $_POST['login'];
    $pass = $_POST['pass'];
    $email = $_POST['email'];
    $age = $_POST['age'];
    $phone = $_POST['phone'];
    $location = $_POST['location'];
    $isAdmin = intval($_POST['isadmin']);


    $user = $DAO->getUser($login);


    if($user->login == $login){
        echo "Uzytkownik o takim loginie ". $login ." już istnieje";


        echo '</br> <a href="index.php">Przejdź do strony głównej!<a/>';
        echo '</br> <a href="formAdd.php">Dodaj kolejnego użytkownika!<a/>';
        die;
    }


    $userToAdd = new User();
    $userToAdd->login = $login;
    $userToAdd->pass = $pass;

    //dodawanie użytkownika do API
    $url = 'http://tank.iai-system.com/api/user/add';

    $post=[];
    $post['login']=$userToAdd->login;
    $post['password']=$userToAdd->pass;
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
        $post);
    $response =  curl_exec($ch);
    $json = json_decode($response, true);
    if(is_array($json)) {
        echo 'API: Dodano użytkownika o loginie ' . $json['login'];
        echo '</br>';

        //dalsza czesc dodawania uzytkownika do bazy danych
        $result = $DAO->addUser($userToAdd);

        if ($result) {
            echo "Uzytkownik {$userToAdd->login} dodany prawidlowo";
        } else {
            echo 'Wystąpił błąd zapytania </br>'. $myDB->getError();
            var_dump($myDB->getErrorList());
        }

    } else {
        echo 'API: Nie udało się dodać użytkownika';
        echo '</br>';
        echo $response;
        echo '</br>';
    }

    if(curl_error($ch)){
        echo curl_error($ch);
    }
    curl_close($ch);
    //koniec dodawania użytkownika do API

} else {
    echo 'Nie podano danych!';
}
echo '</br> <a href="index.php">Przejdź do strony głównej!<a/>';
echo '</br> <a href="formAdd.php">Dodaj kolejnego użytkownika!<a/>';
