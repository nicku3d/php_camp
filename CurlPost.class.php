<?php

/**
 * Class CurlPost
 */
class CurlPost{
    //TODO: verify potrzebuje tylko loginu i klucza, później można przypisać
    //todo: zwracanie obiektów lub tablic konkretnych wartości zamiast wyniku curla!!!
    private $error;
    /**
     * Inizialize curl connection and return its handler
     * @param string $url
     * @return false|resource
     */
    private function curlInit(string $url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        return $ch;
    }

    /**
     * @param $ch
     * @param array $post
     * @return array|mixed
     */
    private function curlExec($ch, $post=[]){
        if(!empty($post)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        $response = curl_exec($ch);

        if(curl_error($ch)){
            $this->error = curl_error($ch);
        }

        curl_close($ch);

        $json = json_decode($response, true);

        if(is_array($json)) {
            //sukces
            return $json;
        } else {
            //podano błędne dane
            return [];
        }
    }

    public function addUser($login, $password){
        $ch = $this->curlInit('http://tank.iai-system.com/api/user/add');
        $post = [
            'login' => $login,
            'password' => $password,
        ];
        return $this->curlExec($ch, $post);

    }

    public function verifyUser($login, $key){
        $ch = $this->curlInit('http://tank.iai-system.com/api/user/verify');
        $post = [
            'login' => $login,
            'key' => $key,
        ];
        return $this->curlExec($ch, $post);
    }

    public function editUser($login, $key, int $status){
        $ch = $this->curlInit('http://tank.iai-system.com/api/user/edit');
        $post = [
            'login' => $login,
            'key' => $key,
            'status' => $status,
            /*
             * 'new_password'=>'',
             * 'icon'=>'',
             */
        ];
        return $this->curlExec($ch, $post);
    }

    public function getAllUsers(){
        $ch = $this->curlInit('http://tank.iai-system.com/api/user/getAll');
        return $this->curlExec($ch);
    }

    public function getActiveChats($login, $key){
        $ch = $this->curlInit('http://tank.iai-system.com/api/chat/getActive');
        $post = [
            'login' => $login,
            'key' => $key,
        ];

        //TODO: stworzyć tablice Chat.class która bedzie zwracana przez tą funkcje
        return $this->curlExec($ch, $post);
    }

    public function createChat($login, $key){
        $ch = $this->curlInit('http://tank.iai-system.com/api/chat/create');
        $post = [
            'login' => $login,
            'key' => $key,
            'name' => 'Chat'/*. $this->chatCount*/,
        ];
        //return $this->curlExec($ch, $post);

        $json = $this->curlExec($ch, $post);
        if (empty($json)) {
            throw new RuntimeException("CurlPost - ERROR:createChat() failure");
        }
        return intval($json['id']);
    }

    public function joinChat($login, $key, $user, $chatID){
        $ch = $this->curlInit('http://tank.iai-system.com/api/chat/join');
        $post = [
            'login' => $login,
            'key' => $key,
            'user'=> $user,
            'chat_id' => $chatID,
        ];
        return $this->curlExec($ch, $post);
    }

    public function leaveChat($login, $key, $chatID){
        $ch = $this->curlInit('http://tank.iai-system.com/api/chat/leave');
        $post = [
            'login' => $login,
            'key' => $key,
            'chat_id' => $chatID,
        ];
        return $this->curlExec($ch, $post);
    }

    public function sendMessage($login, $key, $chatID, $message){
        $ch = $this->curlInit('http://tank.iai-system.com/api/chat/send');
        $post = [
            'login' => $login,
            'key' => $key,
            'chat_id' => $chatID,
            'message' => $message,
        ];
        return $this->curlExec($ch, $post);

    }

    public function getMessages($login, $key/*, $lastID*/){
        $ch = $this->curlInit('http://tank.iai-system.com/api/chat/get');
        $post = [
            'login' => $login,
            'key' => $key,
            //'last_id' => $lastID,
        ];
        return $this->curlExec($ch, $post);
    }

    /**
     * Get error
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }
}