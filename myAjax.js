function sendMessage() {
    $.post("chatController.php", {
        message: document.getElementById("message").value,
        postPurpose: 'sendMessage'
    }).done(function(data) {
        //wypełnia diva o danym id, zwróconymymi danymi
        $("#messages").html(data);
        //czysci pole wiadomosci
        document.getElementById("message").value = '';
        console.log(data);
    });
}

function setActiveChat(chatID){
    let data = {
        chatID: chatID,
        postPurpose: 'setActiveChat'
    };

    function success(data) {
        //wypełnia diva o danym id, zwróconymymi danymi
        $("#chat-users").html(data.chatUsers);
        $(".open-chats").html(data.chat);
        $("#messages").html(data.messages);
        console.log("messages: " + data.messages);
        console.log("Chat: "+data.chat);
        console.log("users: " + data.chatUsers);
    }
    $.post(
        "chatController.php",
        data,
        success,
        'json',
    );
}

function createAndJoin(user){
    let data = {
        user: user,
        postPurpose: 'createAndJoin'
    };
    function success(data) {
        //wypełnia diva o danym id, zwróconymymi danymi
        $("#chat-users").html(data.chatUsers);
        $(".open-chats").html(data.chat);
        $("#messages").html(data.messages);
        console.log("users: " + data.chatUsers);
        console.log("Chat: "+data.chat);
        console.log("Messages: "+data.messages);
    }
    $.post(
        "chatController.php",
        data,
        success,
        'json',
    );

}

function leaveChat(chatID){
    $.post("chatController.php", {
        chatID: chatID,
        postPurpose: 'leaveChat'
    }).done(function(data) {
        //wypełnia diva o danym id, zwróconymymi danymi
        $(".open-chats").html(data);
        console.log(data);
    });
}

function getMessages() {
    $.post("chatController.php", {
        postPurpose: 'getMessages'
    }).done(function(data) {
        //wypełnia diva o danym id/danej klasie, zwróconymymi danymi
        $("#messages").html(data);
        //czysci pole wiadomosci
        console.log("Interval strikes again");
    });
}

function closeChat(chatID){

    let data = {
        chatID: chatID,
        postPurpose: 'closeChat'
    };
    function success(data) {
        //wypełnia diva o danym id, zwróconymymi danymi
        $("#chat-users").html(data.chatUsers);
        $(".open-chats").html(data.chat);
        $("#messages").html(data.messages);
        console.log("users: " + data.chatUsers);
        console.log("Chat: "+data.chat);
        console.log("Messages: "+data.messages);
    }

    $.post(
        "chatController.php",
        data,
        success,
        'json',
    );
}