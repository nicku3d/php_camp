<?php
require_once 'init.php';



$curl = new CurlPost();
$json = $curl->getActiveChats($_SESSION['login'], $_SESSION['key']);


$chatList = [];
if(!empty($json)){
    foreach($json as $chat){
        $tmp = new Chat();
        $tmp->id =  $chat['id'];
        $tmp->name = $chat['name'];
        $tmp->users = $chat['users'];
        $chatList[] = $tmp;
    }
} else {
    echo 'Błąd o 00:16';
}

echo 'Uczestniczyłeś w chatach:';
echo '</br>';
foreach($chatList as $chat){
    echo 'chat_id: '. $chat->id;
    echo '</br>';
    echo 'chat_name: '. $chat->name;
    echo '</br>';
    echo 'chat_users: ';
    if(!empty($chat->users)) {
        foreach ($chat->users as $user) {
            echo $user;
            echo ' ';
        }
    } else {
        echo 'none';
    }
    echo '</br>';
}


//wychodzenie ze wszystkich czatów:

foreach($chatList as $chat) {

    $json = $curl->leaveChat($_SESSION['login'], $_SESSION['key'], $chat->id);
    if (!empty($json)) {
        echo $json['status'];
    } else {
        echo 'Błąd';
        break;
    }
}
