<?php

require_once 'init.php';

//var_dump($_POST);

$myDB = new Database();
$DAO = new UserDAO($myDB);

if(!empty($_POST['login'])) {

    $login = $_POST['login'];
    $result = $DAO->deleteUser($login);

    if ($result) {
        echo 'usunieto uzytkownika ' . $_POST['login'];
        if($_SESSION['login'] == $_POST['login']){
            unset($_SESSION['login']);
        }
        echo '</br> <a href="index.php">Przejdź do strony głównej!<a/>';
    } else {
        echo 'Wystąpił błąd zapytania </br>'. $myDB->getError();
        var_dump($myDB->getErrorList());
    }
}else {
    echo '</br> <a href="userList.php">Aby usunąć lub zmienić dane użytkownika przejdź tutaj!<a/>';
}