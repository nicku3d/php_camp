<?php

$host = 'localhost';
$dbUser = 'root';
$dbPass = '';
$dbName = 'phpcamp';

$db = new mysqli($host, $dbUser, $dbPass, $dbName);

if($db->connect_error) {
    var_dump($db->connect_error);
}