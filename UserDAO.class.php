<?php

class UserDAO
{
    private $db;

    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    /**
     * @return User[]
     */
    public function getAll() : array
    {
        $query = 'SELECT * FROM users';
        $rows = $this->db->getRows($query);
        $userObjects = [];
        foreach($rows as $row) {
//            $userObject = new User();
//            $userObject->login = $row['login'];
//            $userObject->pass = $row['pass'];
//            $userObjects[] = $userObject;

            $userObjects[] = $this->getUserObject($row);
        }

        return $userObjects;
    }

    public function getUser(string $login) : User
    {
        $query = 'SELECT * FROM users WHERE login = "' . $login . '"';
        $row = $this->db->getRow($query);

        return $this->getUserObject($row);
    }

    private function getUserObject($row) : User
    {
        $userObject = new User();
        if(!empty($row)) {
            $userObject->login = $row['login'];
            $userObject->pass = $row['pass'];
            $userObject->email = $row['email'];
            $userObject->age = $row['age'];
            $userObject->phone = $row['phone'];
            $userObject->location = $row['location'];
            $userObject->isAdmin = $row['isAdmin'];
        }

        return $userObject;
    }

    public function addUser(User $user){
        $query = "INSERT INTO `users` (`login`, `pass`) VALUES ( '{$user->login}', '{$user->pass}');";

        return $this->db->query($query);
    }

    public function deleteUser(string $login) {

        $query = 'DELETE FROM `users` WHERE `users`.`login` = "' . $login . '"';
        return $this->db->query($query);
    }

    public function editUser(User $user) {
        $query = "UPDATE users SET email = '{$user->email}'
        , age = '{$user->age}' 
        , phone = '{$user->phone}'
        , location = '{$user->location}'
        WHERE login = '{$user->login}'";

        return $this->db->query($query);
    }
}
