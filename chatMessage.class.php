<?php
/*
  array (size=5)
          'id' => string '859' (length=3)
          'login' => string 'Mikolaj2' (length=8)
          'chat_id' => string '208' (length=3)
          'message' => string 'x' (length=1)
          'date' => string '2020-06-20 00:21:36' (length=19)
 */
class chatMessage{
    public $id;
    public $login;
    public $chatID;
    public $message;
    public $date;
}
