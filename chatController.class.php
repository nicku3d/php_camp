<?php
require_once 'init.php';


//TODO: Add exceptions
/**
 * Class chatController
 * Tworzy/usuwa czaty -> jeden na użytkownika oraz wysyła do nich wiadomości
 */
class chatController{

    private CurlPost $curl;
    private string $login;
    private string $key;


    function __construct($login, $key)
    {
        $this->curl= new CurlPost();
        $this->login = $login;
        $this->key = $key;
    }

    function refreshChatList(){
        $chatList = $this->getChatList();
        $chatListString='';

        if(isset($_SESSION['openChats']) && !empty($_SESSION['openChats'])){
            $openChats = $_SESSION['openChats'];
            //jeśli jakieś czaty są otwarte
            foreach ($chatList as $chat) {

                if (in_array($chat->id, $openChats)) {
                    $chatInfo = '';
                    $chatListString .= '<div class="open-chat position-relative">';
                    $chatInfo .= 'ID:' . $chat->id . ' ';

                    if ($chat->users != null) {
                        foreach ($chat->users as $user) {
                            $chatInfo .= $user;
                            $chatInfo .= ' ';
                        }
                    } else {
                        $chatInfo .= 'none';
                    }
                    $chatListString .= '<button  type="button" class="btn btn-light" id="setActiveBtn" onclick="setActiveChat(' . $chat->id . ')" >'.$chatInfo.'</button>';
                    $chatListString .= '<button  type="button" class="btn btn-danger" id="leaveBtn" onclick="closeChat(' . $chat->id . ')" >x</button>';
                    $chatListString .= '</div>';
                }
            }
        } else {
            $chatListString = "brak otwartych czatów";
        }
        return $chatListString;
    }

    function refreshAllChatList(){
        $chatList = $this->getChatList();
        $chatListString='';
        foreach($chatList as $chat){
            $chatListString .= '<div class="open-chat">';
            $chatListString .= 'ID:' . $chat->id . ' ';
            if ($chat->users != null) {
                foreach ($chat->users as $user) {
                    $chatListString .= $user;
                    $chatListString .= ' ';
                }
            } else {
                $chatListString .= 'none';
            }
            $chatListString .= '<button type="button" id="setActiveBtn" onclick="setActiveChat(' . $chat->id . ')" >aktywuj</button>';
            $chatListString .= '<button type="button" id="leaveBtn" onclick="leaveChat(' . $chat->id . ')" >x</button>';
            $chatListString .= '</div>';
        }
        return $chatListString;
    }

    /**
     * Gets chat messages and returns it
     * @return string
     */
    function refreshMessages()
    {
        $chatID = $this->getActiveChatID();
        $msgList = $this->getMsgList();
        $msgString = '';

        if ($chatID == -1) {
            //jeśli aktywny czat nie jest ustawiony zwróć pusty string
            return $msgString;
        }
        foreach ($msgList as $msg) {
            if ($msg->chatID == $chatID) {
                if (strcmp($this->login, $msg->login) == 0) {
                    //zalogowany
                    $tmpMsg = '<div class="d-flex justify-content-end mb-5">
                            <div class="msg-send">
                                <span class="msg-user-send">Ty</span>
                                ' . $msg->message . '
                                <span class="msg-time-send">' . $msg->date . '</span>
                            </div>
                        </div>';
                } else {
                    //rozmówca
                    $tmpMsg = '<div class="d-flex justify-content-start mb-5">
                            <div class="msg">
                                <span class="msg-user">' . $msg->login . '</span>
                                ' . $msg->message . '
                                <span class="msg-time">' . $msg->date . '</span>
                            </div>
                        </div>';
                }
                $msgString = $tmpMsg . $msgString;
            }
        }

        return $msgString;
    }

    function refreshChatUsers(){
        $chatID = $this->getActiveChatID();

        if($chatID == -1){
            return 'brak użytkowników czatu';
        }
        $chatList = $this->getChatList();
        foreach($chatList as $chat){
            if($chat->id == $chatID){
                return 'rozmowa z: '.implode(" ", $chat->users);
            }
        }

    }

    function refreshAllUsers(){
        $allUsers = $this->curl->getAllUsers();
        $allUsersString = '';
        foreach($allUsers as $user){
            $login = $user['login'];
            $arr = array('.', '\'','"');
            if(strcmp($login, $_SESSION['login']) == 0 || in_array($login[0], $arr)){
                continue;
            }
            $allUsersString .='<tr>';
            //$allUsersString .= '<td>'.$login. '</td>';
            //$allUsersString .=  'td>'. $user['status'] . '</td>';
            $allUsersString .= '<td> 
                <button type="button" class="btn btn-light" id="createBtn" onclick="createAndJoin('.'\''.$login.'\''.')" >'.$login.'</button>
                </td> </tr>';
        }
        return $allUsersString;
    }

    function createChat($user){
        $chatID = $this->ifChatExist($user);

        if($chatID != -1){
            //jeżeli z danym użytkownikiem już istnieje, zwróć id tego czatu
            //TODO: jeśli chat jest już stworzony nie trzeba do niego dodawać nowego użytkownika, jednak nadal należy go otworzyć i uczynić aktywnym
            return $chatID;
        }

            return $this->curl->createChat($this->login, $this->key);
    }

    function joinChat($user, $chatID){
        $this->curl->joinChat($this->login, $this->key, $user, $chatID);

        if (!empty($json)) {
            return true;
        }
        return false;
    }

    function leaveChat($chatID){
        if($chatID == $this->getActiveChatID()){
            //jeśli czat jest aktualnie aktywny zmień id aktywnego czatu
            $newID = -1;
            $this->setActiveChat($newID);
        }
        $this->curl->leaveChat($this->login, $this->key, $chatID);
    }

    function openChat($chatID){
        if(!in_array($chatID, $_SESSION['openChats'])){

            $_SESSION['openChats'][] = $chatID;
        }
        $this->setActiveChat($chatID);
    }

    function closeChat($chatID){
        if(($key = array_search($chatID ,$_SESSION['openChats'])) >= 0){
            if($chatID == $this->getActiveChatID()){
                $this->setActiveChat(-1);
            }
            unset($_SESSION['openChats'][$key]);
            return true;
        }
        return false;
    }

    /**
     * Send message to activeChat if it exists, returns false at failure
     * @param $msg
     * @return bool
     */
    function sendMessage($msg){
        $chatID = $this->getActiveChatID();
        if($chatID == -1){
            return false;
        }
        $this->curl->sendMessage($this->login, $this->key, $chatID, $msg);
        return true;
    }

    function setActiveChat($chatID){
        $_SESSION['activeChatID'] = $chatID;
    }

    /**
     * Returns chatID if it exists, returns -1 if it does not
     * @param $user
     * @return int
     */
    function ifChatExist($user){
        $chatList = $this->getChatList();
        if(!empty($chatList)){
            foreach($chatList as $chat){

                if( !empty($chat->users[0])
                    && strcmp($chat->users[0], $user) == 0){
                    //jeśli istnieją jacyś użytkownicy w tym czacie
                    //i jeśli użytkownik 1 jest równy użytkowniki dla którego tworzymy czat
                    // zwróć ten czat
                    return $chat->id;
                }
            }
        }
        return -1;
    }

    /**
     * If it doesn't get chats from API, it will return empty table
     * @return array
     */
    function getChatList(){

        $userChats = $this->curl->getActiveChats($this->login, $this->key);
        $chatList = [];
        if(!empty($userChats)) {
            foreach ($userChats as $chat) {
                $tmpChat = new Chat();
                $tmpChat->id = $chat['id'];
                $tmpChat->name = $chat['name'];
                $tmpChat->users = $chat['users'];
                $chatList[] = $tmpChat;
            }
        }
        return $chatList;
    }

    function getMsgList(){
        $userMessages = $this->curl->getMessages($this->login, $this->key);
        $msgList=[];
        if(!empty($userMessages)){
            foreach($userMessages['list'] as $msg){
                $tmpMsg = new chatMessage();
                $tmpMsg->id=$msg['id'];
                $tmpMsg->login=$msg['login'];
                $tmpMsg->chatID=$msg['chat_id'];
                $tmpMsg->message=$msg['message'];
                $tmpMsg->date=$msg['date'];
                $msgList[] = $tmpMsg;
            }
        }
        return $msgList;
    }

    function getActiveChatID(){
        if(isset($_SESSION['activeChatID'])){
            return $_SESSION['activeChatID'];
        }
        return -1;
    }

    function clearChats(){
        //leave all chats with 0 users
        $chatList = $this->getChatList();
        foreach($chatList as $chat){
            if(empty($chat->users)){
                $chatID = $chat->id;
                if(!empty($_SESSION['openChats'])
                    && in_array($chatID,$_SESSION['openChats'])){
                    unset($_SESSION['openChats'][$chatID]);
                }
                $this->leaveChat($chatID);
            }
        }

    }

    function leaveAll(){
        $chatList = $this->getChatList();
        foreach($chatList as $chat){
            $chatID = $chat->id;
            if(in_array($chatID,$_SESSION['openChats'])){
                unset($_SESSION['openChats'][$chatID]);
            }
            $this->leaveChat($chatID);
        }
    }


}
